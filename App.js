/** REACT LIBRARY */
import React from 'react';

/**  NAVIGATION */
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

/** SCREENS */
import Home from './src/screens/Home/Home.compoent';
import Settings from './src/screens/Settings/Settings.component';
import Istoric from './src/screens/Istoric/Istoric.component';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {connected: false};
  }

  componentDidMount() {}

  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator initialRouteName="Home">
          <Tab.Screen name="Home" component={Home} />
          <Tab.Screen name="Settings" component={Settings} />
          <Tab.Screen name="Istoric" component={Istoric} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;
