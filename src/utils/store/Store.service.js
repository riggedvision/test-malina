import AsyncStorage from '@react-native-community/async-storage';

const storeUserData = async (key, data) => {
  try {
    await AsyncStorage.setItem(key, data);
  } catch (error) {}
};

const getUserData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    } else {
      return '';
    }
  } catch (error) {}
};

export {storeUserData, getUserData};
