export default {
  timestamp: 'TIMESTAMP',
  latest: 'LATEST',
  currency: 'CURRENCY',
  interval: 'TIMEINTERVAL',
};
