/** REACT LIBRARY */
import React, {Component} from 'react';

/** REACT COMPONENTS  */
import {View, Text} from 'react-native';

import {getHistory, getEURO} from './Istoric.service';

import Chart from './Chart.component';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {ron: [0], eur: [1, 1, 1, 1, 1, 1, 1, 1], usd: [0]};
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const getAll = await getHistory();

    const ero = await getEURO();

    const ron = [];
    const usd = [];

    const rates = getAll.rates;

    const euroRat = ero.rates;

    Object.keys(rates).forEach(function (k) {
      ron.push(rates[k].RON);
      usd.push(rates[k].USD);
    });

    this.setState({
      ron,
      usd,
    });
  }

  render() {
    return (
      <View>
        <Text>RON</Text>
        <Chart data={this.state.ron} />
        <Text>EURO</Text>
        <Chart data={this.state.eur} />
        <Text>USD</Text>
        <Chart data={this.state.usd} />
      </View>
    );
  }
}
