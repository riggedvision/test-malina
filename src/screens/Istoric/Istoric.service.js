import RestService from '../../services/Rest.service';

const getHistory = async () => {
  url = getRequest();
  const history = await RestService.get({
    url,
  });

  return history;
};

const getEURO = async () => {
  url = getRequest() + '&base=RON';
  const history = await RestService.get({
    url,
  });

  return history;
};

getRequest = () => {
  const start =
    new Date().getFullYear() +
    '-' +
    (new Date().getMonth() + 1) +
    '-' +
    new Date().getDate();
  const end = new Date();
  end.setDate(end.getDate() - 10);

  const endD =
    end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate();
  let url = `history?start_at=${endD}&end_at=${start}`;

  return url;
};

export {getHistory, getEURO};
