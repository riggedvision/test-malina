/** REACT LIBRARY */
import React, {Component} from 'react';

import NetInfo from '@react-native-community/netinfo';

/** REACT COMPONENTS  */
import {FlatList, Text, View} from 'react-native';

/** SERVICES */
import {getLatest} from './Home.service';

/** STORAGE */
import {getUserData} from '../../utils/store/Store.service';
import StoreConstants from '../../utils/store/Store.constants';

/** SCREEN COMPONENTS  */
import Row from './Row.component';

let timer = null;

export default class Home extends Component {
  constructor(props) {
    super(props);

    /** DEFAULT VALUES FOR COMPOENT */
    this.state = {
      exchangeList: [],
      timestamp:
        getUserData(StoreConstants.timestamp) !== ''
          ? getUserData(StoreConstants.timestamp)
          : new Date().getTime(),
      isConnected: false,
    };
  }

  /** COMPONENT LIFECICLE */
  componentDidMount() {
    /** LISTENER FOR FOCUS AND BLUR
     * GET'S TRIGGER WHEN YOU ENTER AND LEAVE THE SCREEN
     */
    this.props.navigation.addListener('focus', () => {
      this.populateScreen();
      this.registerTimer();
      this.checkInternet();
    });

    this.props.navigation.addListener('blur', () => {
      this.unregisterTimer();
    });
  }

  /** CHECK IF YOU ARE CONNECTED TO THE INTERNET */
  checkInternet() {
    NetInfo.fetch().then((state) => {
      this.setState({
        isConnected: state.isConnected,
      });
    });
  }

  /** REGISTER APP TIMER */
  async registerTimer() {
    const interval = await getUserData(StoreConstants.interval);
    if (timer == null) {
      timer = setInterval(() => {
        this.populateScreen();
      }, interval * 1000);
    }
  }

  /** DESTROY TIMER */
  unregisterTimer() {
    if (timer) {
      clearInterval(timer);
    }
  }

  /** CALL BACKEND AND MAP DATA */
  async populateScreen() {
    /** SCREEN SERVICE CALL */
    const exchange = await getLatest();
    const exchangeList = [];

    /** MAP DATA */
    Object.keys(exchange.rates).forEach(function (k) {
      exchangeList.push({
        currency: k,
        value: exchange.rates[k],
      });
    });

    /** PROPAGETE CHANGES */
    this.setState({
      exchangeList,
      timestamp: new Date().getTime(),
    });
  }

  render() {
    return (
      <View>
        <Text>{'Timestamp: ' + this.state.timestamp}</Text>
        <Text>{'User connection status: ' + this.state.isConnected}</Text>

        <FlatList
          data={this.state.exchangeList}
          renderItem={({item}) => (
            <Row currency={item.currency} value={item.value} />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
