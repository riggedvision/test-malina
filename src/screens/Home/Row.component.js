/** REACT LIBRARY */
import React, {Component} from 'react';

/** REACT COMPONENTS  */
import {StyleSheet, Text, View} from 'react-native';

export default class HomeRow extends Component {
  render() {
    /** GETTING DATA FROM PARENT */
    const currency = this.props.currency;
    const value = this.props.value;

    return (
      <View style={styles.container}>
        <Text>{currency + ' - ' + value}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
});
