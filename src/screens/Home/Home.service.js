import RestService from '../../services/Rest.service';
import {storeUserData, getUserData} from '../../utils/store/Store.service';
import StoreConstants from '../../utils/store/Store.constants';

const getLatest = async () => {
  const currency = await getUserData(StoreConstants.currency);

  let url = 'latest';

  if (currency != '') {
    url += `?base=${currency}`;
  }
  const latest = await RestService.get({
    url,
  });

  if (latest.rates) {
    storeUserData(StoreConstants.latest, JSON.stringify(latest));
    storeUserData(StoreConstants.timestamp, new Date().getTime() + '');
  } else {
    latest = getUserData(StoreConstants.latest);
  }

  return latest;
};

export {getLatest};
