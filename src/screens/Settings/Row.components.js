/** REACT LIBRARY */
import React, {Component} from 'react';

import CheckBox from '@react-native-community/checkbox';

/** REACT COMPONENTS  */
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';

export default class HomeRow extends Component {
  render() {
    /** GETTING DATA FROM PARENT */
    const currency = this.props.currency;
    const setChecked = this.props.setChecked;
    const index = this.props.index;

    return (
      <View style={styles.container}>
        <CheckBox disabled={true} value={this.props.value} />
        <TouchableOpacity
          onPress={() => {
            setChecked(index, currency);
          }}>
          <Text>{currency}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
});
