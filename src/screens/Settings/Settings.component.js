/** REACT LIBRARY */
import React, {Component} from 'react';

/** REACT COMPONENTS  */
import {Text, View, FlatList, StyleSheet} from 'react-native';

import {storeUserData, getUserData} from '../../utils/store/Store.service';
import StoreConstants from '../../utils/store/Store.constants';

import Row from './Row.components';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      exchangeList: [],
      checked: -1,
      timeList: ['3', '5', '10'],
      timeChecked: -1,
    };
  }

  /** COMPONENT LIFECICLE */
  componentDidMount() {
    this.populateScreen();

    this.setChecked = this.setChecked.bind(this);
    this.setTimeChecked = this.setTimeChecked.bind(this);
  }

  async populateScreen() {
    let exchange = await getUserData(StoreConstants.latest);
    let crc = await getUserData(StoreConstants.currency);

    const interval = await getUserData(StoreConstants.interval);
    let intervalChecked = -1;
    if (interval == '') {
      intervalChecked = 1;
    } else {
      for (let i = 0; i < this.state.timeList.length; i++) {
        if (this.state.timeList[i] == interval) {
          intervalChecked = i;
        }
      }
    }

    let found = false;
    const exchangeList = [];

    if (exchange !== '') {
      /** MAP DATA */
      exchange = JSON.parse(exchange);

      Object.keys(exchange.rates).forEach(function (k) {
        exchangeList.push({
          currency: k,
          value: exchange.rates[k],
        });

        if (k === crc) {
          found = true;
          crc = exchangeList.length - 1;
        }
      });

      /** PROPAGETE CHANGES */

      console.log(intervalChecked);
      this.setState({
        exchangeList,
        checked: found ? crc : -1,
        timeChecked: intervalChecked,
      });
    }
  }

  setChecked(index, value) {
    this.setState({checked: index});
    storeUserData(StoreConstants.currency, value);
  }

  setTimeChecked(index, value) {
    this.setState({
      timeChecked: index,
    });
    storeUserData(StoreConstants.interval, value);
  }

  render() {
    return (
      <View>
        <Text> CURRENCY REQUEST: </Text>
        <FlatList
          style={styles.list}
          data={this.state.exchangeList}
          renderItem={({item, index}) => (
            <Row
              currency={item.currency}
              value={index === this.state.checked}
              setChecked={this.setChecked}
              index={index}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
        <Text>TIME SELECT</Text>
        <FlatList
          style={styles.list}
          data={this.state.timeList}
          renderItem={({item, index}) => (
            <Row
              currency={item}
              value={index === this.state.timeChecked}
              setChecked={this.setTimeChecked}
              index={index}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    height: '30%',
  },
});
