export default class RestService {
  /* post methode */
  static post({url, data, headers}) {
    return this.request({
      url,
      data,
      headers,
      method: 'POST',
    });
  }

  /* get methdode */
  static get({url, data, headers}) {
    return this.request({
      url,
      data,
      headers,
      method: 'GET',
    });
  }

  /** REQUEST METHODE */
  static async request({method, url, data, headers}) {
    /** CREATING HEADERS REQUEST */
    const requestHeaders = new Headers();

    /** CREATING BODY REQUEST */
    let body = data;

    /* SETTING PREIMPLICIT HEADERS */
    requestHeaders.set('Content-Type', 'application/json');

    /* SETTING PARAMETER HEADER */
    for (const key in headers) {
      requestHeaders.set(key, headers[key]);
    }

    /** SETTING BODY */
    if (data != undefined) {
      body = JSON.stringify(data);
    }

    /** REQUEST BASE URL */
    const baseUrl = 'https://api.exchangeratesapi.io';

    let options = {};
    if (method === 'POST') {
      options = {
        method,
        headers: requestHeaders,
        body,
      };
    } else {
      options = {
        method,
        headers: requestHeaders,
      };
    }

    return fetch(`${baseUrl}/${url}`, options)
      .then((response) => {
        return response.json();
      })
      .catch((e) => {
        return {};
      })
      .then((responseContent) => {
        return responseContent;
      });
  }
}
